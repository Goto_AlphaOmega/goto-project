{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Types where

import Data.Proxy
import Data.ByteString (ByteString)
import Data.Text (Text)
import Servant.API

type API = "send" :> QueryParam "message" Text :> QueryParam "user" Text :> Get '[JSON] NoContent
      :<|> "receive" :> ReqBody '[OctetStream] ByteString :> Post '[JSON] NoContent
      :<|> "chat" :> Get '[JSON] [Text]

api :: Proxy API
api = Proxy
