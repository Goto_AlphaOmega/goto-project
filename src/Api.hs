{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE OverloadedStrings #-}

module Api
  ( server
  ) where

import Network.HTTP.Client
import Servant (Server, Handler)
import Servant.Client
import Servant.API
import Control.Monad
import Control.Monad.IO.Class
import Control.Concurrent.Async
import Database.PostgreSQL.Simple
import Data.Maybe
import Data.Text (Text)
import Data.Text.Encoding (encodeUtf8, decodeUtf8With)
import Data.ByteString (ByteString)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as C
import Crypto.Saltine.Core.Box
import Crypto.Saltine.Class (encode, decode)
import Crypto.Saltine.Internal.ByteSizes (boxNonce)
import Types
import Client

hosts = [("localhost", 8080)]

server :: Connection -> Keypair -> Server API
server conn (sk, pk) = sendMessage :<|> recvMessage :<|> getMessages
  where sendMessage :: Maybe Text -> Maybe Text -> Handler NoContent
        sendMessage msg' user' = do
          let msg = fromJust msg'
          let user = fromJust user'
          liftIO $ do
            manager <- newManager defaultManagerSettings
            forConcurrently_ hosts $ \(h, p) -> do
              n <- newNonce
              flip runClientM (mkClientEnv manager (BaseUrl Http h p "")) $ receive $
                B.append (encode n) (box pk sk n ( B.append msg "$" user))
          return NoContent

        recvMessage :: ByteString -> Handler NoContent
        recvMessage msg' = do
          -- TODO: parsing galore
          let n = fromJust $ decode $ B.take boxNonce msg'
          let ciphertext = B.drop boxNonce msg'
          let msg = fromJust $ decodeUtf8With (const . const Nothing) <$> boxOpen pk sk n ciphertext

          liftIO . void $ do
            print msg
            withTransaction conn $ execute conn
                    "INSERT INTO messages VALUES (?)"
                    (Only msg)
          return NoContent

        getMessages :: Handler [Text]
        getMessages = fmap (map fromOnly) . liftIO $
            query_ conn "SELECT text FROM messages"
