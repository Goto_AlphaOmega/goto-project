{ mkDerivation, async, base, bytestring, http-client
, postgresql-simple, saltine, servant, servant-client
, servant-server, stdenv, text, wai, warp
}:
mkDerivation {
  pname = "servant-minimal";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  libraryHaskellDepends = [
    async base bytestring http-client postgresql-simple saltine servant
    servant-client servant-server text wai
  ];
  executableHaskellDepends = [
    base http-client postgresql-simple saltine servant-client
    servant-server wai warp
  ];
  homepage = "https://gitlab.com/goto-ru/dissy/servant-minimal";
  license = stdenv.lib.licenses.agpl3;
}
