import React, { Component } from 'react';
import './SendMessage.css'


class SendMessage extends Component {
    constructor(props){
        super(props);
        this.state = {newMessage: ""};
        //  this.props.g(this.state);
    };
    render(){
        return( <div className="SendMessage">
            <textarea className="input-container" value={ this.state.input} onChange={(ev)=>{
                this.setState({newMessage: ev.target.value})

            }
            } />

            <button className="button-container" onClick={() =>
            { console.log(this.props.g(this.state.newMessage) ) }  }> Send </button>

        </div>);
    }

}

export default SendMessage;