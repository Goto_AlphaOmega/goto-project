{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_servant_chat (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/zerogoki/.cabal/bin"
libdir     = "/home/zerogoki/.cabal/lib/x86_64-linux-ghc-8.4.3/servant-chat-0.1.0.0-FRGzxEWodBX5EcrOPwkWHY"
dynlibdir  = "/home/zerogoki/.cabal/lib/x86_64-linux-ghc-8.4.3"
datadir    = "/home/zerogoki/.cabal/share/x86_64-linux-ghc-8.4.3/servant-chat-0.1.0.0"
libexecdir = "/home/zerogoki/.cabal/libexec"
sysconfdir = "/home/zerogoki/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "servant_chat_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "servant_chat_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "servant_chat_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "servant_chat_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "servant_chat_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "servant_chat_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
